package org.zstacks.znet;

import org.zstacks.znet.nio.Dispatcher;
import org.zstacks.znet.ticket.ResultCallback;

public class MyRemotingClient {

	@SuppressWarnings("resource")
	public static void main(String[] args) throws Exception { 
		
		//1)创建Dispatcher
		final Dispatcher dispatcher = new Dispatcher(); 
		//2)建立链接
		final RemotingClient client = new RemotingClient("127.0.0.1:80", dispatcher);
		
		Message msg = new Message();
		msg.setCommand("hello");
		msg.setBody("hello world");
		
		client.invokeAsync(msg, new ResultCallback() {
			@Override
			public void onCompleted(Message result) { 
				System.out.println(result); 
			}
		});
	}
}
