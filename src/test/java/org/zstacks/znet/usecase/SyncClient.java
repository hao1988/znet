package org.zstacks.znet.usecase;


import org.zstacks.znet.Message;
import org.zstacks.znet.RemotingClient;
import org.zstacks.znet.nio.Dispatcher;

public class SyncClient {

	public static void main(String[] args) throws Exception { 
		Dispatcher dispatcher = new Dispatcher();

		final RemotingClient client = new RemotingClient("127.0.0.1:80", dispatcher);
	
		Message msg = new Message();
		msg.setCommand("hello");
		msg.setBody("hello");
		Message res = client.invokeSync(msg); //同步请求
		System.out.println(res);
		
		
		client.close();
		dispatcher.close(); 
	}

}
