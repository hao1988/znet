package org.zstacks.znet.ticket;

import org.zstacks.znet.Message;

 
public interface ResultCallback { 
	public void onCompleted(Message result);  
}
