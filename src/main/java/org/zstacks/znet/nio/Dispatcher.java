package org.zstacks.znet.nio;

import java.io.Closeable;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SelectableChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

 

public class Dispatcher implements Closeable {
	private static final Logger log = LoggerFactory.getLogger(Dispatcher.class); 
	private ExecutorService executor;
	
	private int selectorCount = 1;
	private int executorCount = 4;
	private SelectorThread[] selectors;
	private AtomicInteger selectorIndex = new AtomicInteger(0);
	private String dispatcherName = "Dispatcher";
	private String selectorNamePrefix = "Selector";
	
	
	protected volatile boolean started = false;  
	 
	private IoAdaptor serverIoAdaptor;
	
	private void init() throws IOException{
		ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(executorCount,
				executorCount, 120, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>());
		
		this.executor = threadPoolExecutor;
		this.selectors = new SelectorThread[this.selectorCount];
		for(int i=0;i<this.selectorCount;i++){
			String name = String.format("%s-%s-%d", dispatcherName, selectorNamePrefix, i);
			this.selectors[i] = new SelectorThread(this, name);
		}
	}
	
	public SelectorThread getSelector(int index){
		if(index <0 || index>=this.selectorCount){
			throw new IllegalArgumentException("Selector index should >=0 and <"+this.selectorCount);
		}
		return this.selectors[index];
	}
	
	public SelectorThread nextSelector(){
		return this.selectors[this.selectorIndex.getAndIncrement()%this.selectorCount];
	}

	public void registerChannel(SelectableChannel channel, int ops) throws IOException{
		this.nextSelector().registerChannel(channel, ops);
	}
	
	public void registerSession(int ops, Session sess) throws IOException{
		if(sess.dispatcher() != this){
			throw new IOException("Unmatched Dispatcher");
		}
		this.nextSelector().registerSession(ops, sess);
	}
	
	
	public SelectorThread getSelector(SelectionKey key){
		for(SelectorThread e : this.selectors){
			if(key.selector() == e.selector){
				return e;
			}
		}
		return null;
	}
	
	public synchronized void start() {
		if (this.started) {
			return;
		}
		try {
			this.init();
		} catch (IOException e) {
			log.error(e.getMessage(), e);
			return;
		}
		this.started = true;
		for (SelectorThread dispatcher : this.selectors) {
			dispatcher.start();
		} 
		log.info("{}(SelecctorCount={}) started", this.dispatcherName, this.selectorCount);
	}
	
	public synchronized void stop() {
		if (!this.started)
			return;

		this.started = false;
		for (SelectorThread dispatcher : this.selectors) {
			dispatcher.interrupt();
		} 
		executor.shutdown();
		log.info("{}(SelecctorCount={}) stopped", this.dispatcherName, this.selectorCount);
	} 
	 
	public void close() throws IOException {
		this.stop();
	}
	
	public boolean isStarted(){
		return this.started;
	}
	 
	
	public IoAdaptor serverIoAdaptor(){
		return this.serverIoAdaptor; 
	}
	public Dispatcher serverIoAdaptor(IoAdaptor serverIoAdaptor){ 
		if(this.serverIoAdaptor != null){
			throw new IllegalStateException("Server IoAdaptor already exists");
		}
		this.serverIoAdaptor = serverIoAdaptor;
		return this;
	}
	
	public ExecutorService executorService(){
		return this.executor;
	}
	
	public void asyncRun(Runnable task){
		this.executor.submit(task);
	}
	
	public int selectorCount(){
		return this.selectorCount;
	}
	public Dispatcher selectorCount(int count){ 
		this.selectorCount = count;
		return this;
	}
	public Dispatcher executorCount(int count){ 
		this.executorCount = count;
		return this;
	}
	public Dispatcher name(String name){ 
		this.dispatcherName = name;
		return this;
	}
	
	public ServerSocketChannel registerServerChannel(String host, int port) throws IOException{
		ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
    	serverSocketChannel.configureBlocking(false);
    	serverSocketChannel.socket().bind(new InetSocketAddress(host, port)); 
    	this.registerChannel(serverSocketChannel, SelectionKey.OP_ACCEPT); 
    	return serverSocketChannel;
	}
	
	
	public Session registerClientChannel(String host, int port, IoAdaptor ioAdaptor) throws IOException{
		SocketChannel channel = SocketChannel.open();
    	channel.configureBlocking(false);
    	channel.connect(new InetSocketAddress(host, port)); 
		
    	Session session = new Session(this, channel, ioAdaptor);
    	this.registerSession(SelectionKey.OP_CONNECT, session);
    	
    	return session;
	}
}
